var sqlite3 = require('sqlite3').verbose()

const DBSOURCE = "db.sqlite"

let db = new sqlite3.Database(DBSOURCE, (err) => {
    if (err) {
      // Cannot open database
      console.error(err.message)
      throw err
    }else{
        console.log('Connected to the SQLite database.')
        db.run(`CREATE TABLE bagisci (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            ad text, 
            soyad text,
            tcKimlik INTEGER UNIQUE,
            iletisim INTEGER,
            sag text,
            sartlar text,
            hizmetler text,
            ziyaret text,
            talep text,
            mezarlikBilg text, 
            giderler text,
            sartTarih date,
            sartAciklama text,
            hizmetTarih date,
            hizmetAciklama text,
            ziyaretTarih date,
            ziyaretAciklama text,
            talepTarih date,
            talepAciklama text,
            giderTarih date,
            giderAciklama text,
            giderTutar integer,
            ziyaretFormu , 
            foto ,
            )`,
        (err) => {
            if (err) {
                // Table already created
            }else{
                // Table just created, creating some rows
                var insert = 'INSERT INTO bagisci (ad, soyad, tcKimlik, iletisim, sag, sartlar, hizmetler, ziyaret, talep, mezarlikBilg, giderler, sartTarih, sartAciklama, hizmetTarih, hizmetAciklama, ziyaretTarih, ziyaretAciklama, talepTarih, talepAciklama, giderTarih, giderAciklama, giderTutar) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
                db.run(insert, ["ilke","yas","12344483232","5553446784","sağ","","","","","yok","","","","","","","","","","","",""])
            }
        });  
    }
});


module.exports = db