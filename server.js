var express = require("express")
var cors = require('cors')
var app = express()
app.use(cors())
var db = require("./database.js")

var bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Server port
var HTTP_PORT = 8000 
// Start server
app.listen(HTTP_PORT, () => {
    console.log("Server running on port %PORT%".replace("%PORT%",HTTP_PORT))
});
// Root endpoint
app.get("/", (req, res, next) => {
    res.json({"message":"Ok"})
});

// Insert here other API endpoints


// user list
app.get("/api/bagiscis", (req, res, next) => {
    var sql = "select * from bagisci"
    var params = []
    db.all(sql, params, (err, rows) => {
        if (err) {
          res.status(400).json({"error":err.message});
          return;
        }
        res.json({
            "message":"success",
            "data":rows
        })
      });
});

// id ye göre list
app.get("/api/bagisci/:id", (req, res, next) => {
    var sql = "select * from bagisci where id = ?"
    var params = [req.params.id]
    db.get(sql, params, (err, row) => {
        if (err) {
          res.status(400).json({"error":err.message});
          return;
        }
        res.json({
            "message":"success",
            "data":row
        })
      });
});

// user ekle
app.post("/api/bagisci/", (req, res, next) => {
    var errors=[]
    if (!req.body.ad){
        errors.push("İsim girisi yapılmadı");
    }
    if (!req.body.soyad){
        errors.push("Soyad girisi yapılmadı");
    }
    if (!req.body.tcKimlik){
        errors.push("TC kimlik girisi yapılmadı");
    }
    if (!req.body.iletisim){
        errors.push("Telefon numarası girisi yapılmadı");
    }
    if (!req.body.sag){
        errors.push("Durum girisi yapılmadı");
    }
    if (errors.length){
        res.status(400).json({"error":errors.join(",")});
        return;
    }
    var data = {
        ad: req.body.ad,
        soyad: req.body.soyad,
        tcKimlik: req.body.tcKimlik,
        iletisim: req.body.iletisim,
        sag: req.body.sag,
        sartlar: req.body.sartlar,
        hizmetler: req.body.hizmetler,
        ziyaret: req.body.ziyaret,
        talep: req.body.talep,
        mezarlikBilg: req.body.mezarlikBilg, 
        giderler: req.body.giderler,
        sartTarih: req.body.sartTarih,
        sartAciklama: req.body.sartAciklama,
        hizmetTarih: req.body.hizmetTarih,
        hizmetAciklama: req.body.hizmetAciklama,
        ziyaretTarih: req.body.ziyaretTarih,
        ziyaretAciklama: req.body.ziyaretAciklama,
        talepTarih: req.body.talepTarih,
        talepAciklama: req.body.talep_aciklama,
        giderTarih: req.body.giderTarih,
        giderAciklama: req.body.giderAciklama,
        giderTutar: req.body.giderTutar
    }
    var sql ='INSERT INTO bagisci (ad, soyad, tcKimlik, iletisim, sag, sartlar, hizmetler, ziyaret, talep, mezarlikBilg, giderler, sartTarih, sartAciklama, hizmetTarih, hizmetAciklama, ziyaretTarih, ziyaretAciklama, talepTarih, talepAciklama, giderTarih, giderAciklama, giderTutar) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
    var params =[data.ad, data.soyad, data.tcKimlik, data.iletisim, data.sag, data.sartlar, data.hizmetler, data.ziyaret, data.talep, data.mezarlikBilg, data.giderler, data.sartTarih, data.sartAciklama, data.hizmetTarih, data.hizmetAciklama, data.ziyaretTarih, data.ziyaretAciklama, data.talepTarih, data.talepAciklama, data.giderTarih, data.giderAciklama, data.giderTutar]
    db.run(sql, params, function (err, result) {
        if (err){
            res.status(400).json({"error": err.message})
            return;
        }
        res.json({
            "message": "success",
            "data": data,
            "id" : this.lastID
        })
    });
})
//update
app.put("/api/bagisci/:id", (req, res, next) => {
    var data = {
        ad: req.body.ad ? req.body.ad : null,
        soyad: req.body.soyad ? req.body.soyad : null,
        tcKimlik: req.body.tcKimlik ? req.body.tcKimlik : null,
        iletisim: req.body.iletisim ? req.body.iletisim : null,
        sag: req.body.sag ? req.body.durum : null,
        sartlar: req.body.sartlar ? req.body.sartlar : null,
        hizmetler: req.body.hizmetler ? req.body.hizmetler : null,
        ziyaret: req.body.ziyaret ? req.body.ziyaret : null,
        talep: req.body.talep ? req.body.talep : null,
        mezarlikBilg: req.body.mezarlikBilg ? req.body.mezarlikBilg : null, 
        giderler: req.body.giderler ? req.body.giderler : null,
        sartTarih: req.body.sartTarih ? req.body.sartTarih : null,
        sartAciklama: req.body.sartAciklama ? req.body.sartAciklama : null,
        hizmetTarih: req.body.hizmetTarih ? req.body.hizmetTarih : null,
        hizmetAciklama: req.body.hizmetAciklama ? req.body.hizmetAciklama : null,
        ziyaretTarih: req.body.ziyaretTarih ? req.body.ziyaretTarih : null,
        ziyaretAciklama: req.body.ziyaretAciklama ? req.body.ziyaretAciklama : null,
        talepTarih: req.body.talepTarih ? req.body.talepTarih : null,
        talepAciklama: req.body.talepAciklama ? req.body.talepAciklama : null,
        giderTarih: req.body.giderTarih ? req.body.giderTarih : null,
        giderAciklama: req.body.giderAciklama ? req.body.giderAciklama : null,
        giderTutar: req.body.giderTutar ? req.body.giderTutar : null
    }
 
   if(data.ad != null && data.name.length < 1) data.ad = null;
   if(data.soyad != null && data.email.length < 1) data.soyad = null;

    db.run(
        `UPDATE bagisci set 
           ad = COALESCE(?,ad), 
           soyad = COALESCE(?,soyad), 
           tcKimlik = COALESCE(?,tcKimlik),
           iletisim = COALESCE(?,iletisim), 
           sag = COALESCE(?,sag), 
           sartlar = COALESCE(?,sartlar), 
           hizmetler = COALESCE(?,hizmetler), 
           ziyaret = COALESCE(?,ziyaret), 
           talep = COALESCE(?,talep), 
           mezarlikBilg = COALESCE(?,mezarlikBilg), 
           giderler = COALESCE(?,giderler), 
           sartTarih = COALESCE(?,sartTarih), 
           sartAciklama = COALESCE(?,sartAciklama), 
           hizmetTarih = COALESCE(?,hizmetTarih), 
           hizmetAciklama = COALESCE(?,hizmetAciklama), 
           ziyaretTarih = COALESCE(?,ziyaretTarih), 
           ziyaretAciklama = COALESCE(?,ziyaretAciklama), 
           talepTarih = COALESCE(?,talepTarih), 
           talepAciklama = COALESCE(?,talepAciklama), 
           giderTarih = COALESCE(?,giderTarih), 
           giderAciklama = COALESCE(?,giderAciklama), 
           giderTutar = COALESCE(?,giderTutar)
           WHERE id = ?`,
        [data.ad, data.soyad, data.tcKimlik, data.iletisim, data.sag, data.sartlar, data.hizmetler, data.ziyaret, data.talep, data.mezarlikBilg, data.giderler, data.sartTarih, data.sartAciklama, data.hizmetTarih, data.hizmetAciklama, data.ziyaretTarih, data.ziyaretAciklama, data.talepTarih, data.talepAciklama, data.giderTarih, data.giderAciklama, data.giderTutar, req.params.id],
        function (err, result) {
            if (err){
                res.status(400).json({"error": res.message})
                return;
            }
            res.json({
                message: "success",
                data: data,
                changes: this.changes
            })
    });
})
app.delete("/api/bagisci/:id", (req, res, next) => {
    db.run(
        'DELETE FROM bagisci WHERE id = ?',
        req.params.id,
        function (err, result) {
            if (err){
                res.status(400).json({"error": res.message})
                return;
            }
            res.json({"message":"deleted", changes: this.changes})
    });
})

// Default response for any other request
app.use(function(req, res){
    res.status(404);
});

